import './styles/App.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Home from './components/home/Home';
import NotFound from './components/notFound/NotFound';
import Category from './components/category/Category';
import Difficulty from './components/difficulty/Difficulty';
import Quiz from './components/quiz/Quiz';
import Score from './components/score/Score';


function App() {

  localStorage.setItem(
    "quizApp", JSON.stringify({
        scores : [],
        lastIdentityNumber : 0
      })
  );

  return (
    <BrowserRouter>

      <Routes>

        <Route exact path="/">
          <Route index element={ <Home /> } />
          <Route path="home" element={ <Home /> } />
          <Route path="category" element={ <Category /> } />
          <Route path="difficulty" element={ <Difficulty /> } />
          <Route path="quiz" element={ <Quiz /> } />
          <Route path="score" element={ <Score /> } />
        </Route>

        <Route path="*" element={ <NotFound /> } />
      </Routes>
    </BrowserRouter>

  );
}

export default App;
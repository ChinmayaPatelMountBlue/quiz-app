import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, Link } from 'react-router-dom';
import store from '../../store';

const axios = require('axios');

export default function Category (props) {

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const [categories, setCategories] = useState([]);

    const [categoriesDisplay,setCategoriesDisplay] = useState([]);

    useEffect(() => {

        async function displayCategory() {
            
            const res = (await axios.get("https://opentdb.com/api_category.php"))?.data.trivia_categories;

            setCategories(res);
    
        }

        displayCategory();

    }, []);

    function handleSubmit (categoryId) {

        dispatch({

            type: "ADD_CATEGORY",
            payload: categoryId

        });

        navigate("/difficulty");

    }


    useEffect(() => {

        function display(){

            const tempCategoriesDisplay = categories.map((category) => {
    
                return (
        
                    <li className="category-list-item" key={category.id}>
                        <div className="category-card" onClick={() => handleSubmit(category.id)}>

                            <div className="category-name">{category.name}</div>
                            
        
                        </div>
                    </li>
        
                );
        
            });

        setCategoriesDisplay(tempCategoriesDisplay);

        }

        display();

    }, [categories]);
        
    return (

        <>

        <nav className="nav">

            <ul className="nav-list">

                <li className="nav-link">
                    <Link className="link" to="/">Home</Link>
                </li>

                <li className="nav-name">{store?.getState()?.name}</li>

            </ul>

        </nav>

        <div className="category">

            <h1 className="category-header">Choose your category</h1>

            <div className="category-container">

                <form className="category-form">

                    <div className="category-list-container">

                        <ul className="category-list">

                            <li className="category-list-item">
                                <div className="category-card" onClick={() => handleSubmit("random")}>
                                    <div className="category-name">Random</div>
                                </div>
                            </li>

                            {categoriesDisplay}

                        </ul>

                    </div>

                </form>

            </div>

        </div>
        </>
    );

}
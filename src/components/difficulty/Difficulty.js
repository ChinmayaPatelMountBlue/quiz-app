import { useDispatch } from "react-redux";
import { useNavigate, Link } from "react-router-dom";
import store from "../../store";

export default function Difficulty (props) {

    const axios = require('axios');

    const dispatch = useDispatch();
    const navigate = useNavigate();

    function handleSubmit (difficultyLevel) {

        dispatch({

            type: "ADD_DIFFICULTY",
            payload: difficultyLevel 

        });

        async function getQuestions() {

            const quizStore = store.getState();

            const category = quizStore.category;
            const difficulty = quizStore.difficulty;

            let url = "https://opentdb.com/api.php?amount=10";

            if(category !== "random"){
                url += `&category=${category}`;
            }
            if(difficulty !== "random"){
                url += `&difficulty=${difficulty}`;
            }
            url += `&type=multiple`;

            const jsonData = (await axios.get(url)).data;
            const questions = await jsonData.results;
            if(jsonData.response_code === 0){
    
                dispatch({

                    type: "ADD_QUESTIONS",
                    payload: questions

                });
    
            }

        }

        getQuestions();

        navigate("/quiz");

    }

    return (

        <>

        <nav className="nav">

            <ul className="nav-list">

                <li className="nav-link">
                    <Link className="link" to="/">Home</Link>
                </li>

                <li className="nav-name">{store?.getState()?.name}</li>

            </ul>

        </nav>

        <div className="difficulty-outer-container">

            <h1 className="difficulty-header">
                Choose your Difficulty
            </h1>

            <div className="difficulty-inner-container">

                <div className="difficulty-card easy-card" onClick={() => handleSubmit("easy")}>

                    <h1 className="difficulty-card-header">EASY</h1>

                    <div className="difficulty-card-img-container">
                        <img src={process.env.PUBLIC_URL+"images/difficulty-img.png"} alt="Easy" className="difficulty-card-img" />
                    </div>

                </div>

                <div className="difficulty-card medium-card" onClick={() => handleSubmit("medium")}>

                    <h1 className="difficulty-card-header">MEDIUM</h1>

                    <div className="difficulty-header-img-container">
                        <img src={process.env.PUBLIC_URL+"images/difficulty-img.png"} alt="Random" className="difficulty-card-img" />
                    </div>

                </div>

                <div className="difficulty-card hard-card" onClick={() => handleSubmit("hard")}>

                    <h1 className="difficulty-card-header">HARD</h1>

                    <div className="difficulty-header-img-container">
                        <img src={process.env.PUBLIC_URL+"images/difficulty-img.png"} alt="Hard" className="difficulty-card-img" />
                    </div>

                </div>

                <div className="difficulty-card random-card" onClick={() => handleSubmit("random")}>

                    <h1 className="difficulty-card-header">RANDOM</h1>

                    <div className="difficulty-card-img-container">
                        <img src={process.env.PUBLIC_URL+"images/difficulty-img.png"} alt="Random" className="difficulty-card-img" />
                    </div>

                </div>

            </div>

        </div>
        </>

    );

}
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, Link } from 'react-router-dom';
import store from '../../store';

export default function Home (props) {

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const [name,setName] = useState("");

    function handleChange (event) {

        if(event){

            setName(event.target.value);

        }

    }

    function handleSubmit(event){

        event.preventDefault();

        dispatch({

            type: "ADD_NAME",
            payload: event.target[0].value

        });

        navigate("/category");

    }

    function handleSubmitAnonymous (event) {

        event.preventDefault();

        let storage = JSON.parse(localStorage.getItem("quizApp"));

        const newIdentityNumber = storage.lastIdentityNumber+1;

        const newStorage = {...storage, lastIdentityNumber: newIdentityNumber};

        localStorage.setItem("quizApp",JSON.stringify(newStorage));

        dispatch({

            type: "ADD_NAME",
            payload: "Alien_"+newIdentityNumber

        })

        navigate("/category");

    }

    return (

        <>
        <nav className="nav">

            <ul className="nav-list">

                <li className="nav-link">
                    <Link className="link" to="/">Home</Link>
                </li>

                <li className="nav-name">{store?.getState()?.name}</li>

            </ul>

        </nav>


        <div className="home">

            <div className="login-card">

                <div className="genius-img-container">
                    <img src={process.env.PUBLIC_URL + "/images/login-img.png"} alt="Genius Meme" className="genius-img" />
                    <h1 className="login-header" >Let's Play a Quiz</h1>
                    <h2 className="login-header" >Let Us See if You can answer this first!</h2>
                </div>

                <form className="login-form" onSubmit={handleSubmit} >

                    <div className="name-container">

                        <label htmlFor="name">
                        <input
                            type="text"
                            className="name-input"
                            onChange={handleChange}
                            value={(name && name!=="") ? name : "" }
                            id="name"
                            name="name"
                            placeholder="Can you tell your Name?"
                        />
                        <input
                            type="text"
                            className="name-input-570"
                            onChange={handleChange}
                            value={(name && name!=="") ? name : "" }
                            id="name"
                            name="name"
                            placeholder="What's your Name?"
                        />
                        </label>
                        <input
                            type="text"
                            className="name-input-450"
                            onChange={handleChange}
                            value={(name && name!=="") ? name : "" }
                            id="name"
                            name="name"
                            placeholder="Your Name?"
                        />
                        <input
                            type="text"
                            className="name-input-280"
                            onChange={handleChange}
                            value={(name && name!=="") ? name : "" }
                            id="name"
                            name="name"
                            placeholder="Name?"
                        />

                    </div>

                    <div className={(name && name!== "") ? "confirm-btn-container" : "display-none" }>

                        <button type="submit" className="btn confirm-btn">Confirm User Name?</button>

                    </div>

                </form>

                <form onSubmit={handleSubmitAnonymous}>

                    <input type="text" hidden value="Alien" readOnly />

                    <div className="anonymous-btn-container">

                        <button type="submit" className="anonymous-btn btn">Play Anonymously</button>

                    </div>

                </form>

            </div>

        </div>
        </>
    );

}
import { Link } from "react-router-dom";

export default function NotFound (props) {

    return (

        <div className="not-found">

            <div className="error-page-container">

                <div className="error-page-header">

                    <h2>Error: 404</h2>

                    <h1>Page Not Found</h1>

                    <ol className="error-page-list">

                        <li className="error-page-list-item">

                                <Link to="/" className="link" >Click here to go to Home</Link>

                        </li>
                    </ol>

                </div>

            </div>

        </div>

    );

}
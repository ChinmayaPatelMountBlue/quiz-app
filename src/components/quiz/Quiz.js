import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, Link } from "react-router-dom";
import store from "../../store";

export default function Quiz (props) {

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const [questionsData, setQuestionsData] = useState([]);

    setTimeout(() => {
        setQuestionsData(store?.getState()?.questions);
    },700);

    const [score, setScore] = useState(0);

    const [currentQuestionNumber, setCurrentQuestionNumber] = useState(1);

    const [currentQuestionData, setCurrentQuestionData] = useState({});

    const [currentAnswers, setCurrentAnswers] = useState([]);

    const [currentCorrectAnswer, setCurrentCorrectAnswer] = useState("");

    useEffect(() => {

        function populateQuestion() {

            setTimeout(()=>{
                setCurrentQuestionData(questionsData[(currentQuestionNumber-1)]);
            },500);

        }

        populateQuestion();

    }, [questionsData, currentQuestionNumber]);

    useEffect(() => {

        (function shuffleAnswers () {

            const correctAnswer = currentQuestionData?.correct_answer;

            const incorrectAnswers = currentQuestionData?.incorrect_answers;

            setCurrentCorrectAnswer(correctAnswer);

            const result = incorrectAnswers?.concat(correctAnswer);

            for(let index = 0; index < result?.length; index++) {

                let shufflingIndex = Math.floor(Math.random() * (index+1));

                let temp = result[shufflingIndex];
                result[shufflingIndex] = result[index];
                result[index] = temp;

            }

            setCurrentAnswers(result);

        })();

    }, [currentQuestionData]);

    function handleAnswerSelection (selectedAnswer) {

        if(currentQuestionNumber < 10){

            if(selectedAnswer === currentCorrectAnswer){

                let newScore = ((score === 0) ? 0 : score);
                newScore++;
                setScore(newScore);

            }

            let newCurrentQuestionNumber = currentQuestionNumber;
            newCurrentQuestionNumber++;
            setCurrentQuestionNumber(newCurrentQuestionNumber);

        }
        else if(currentQuestionNumber === 10){

            if(selectedAnswer === currentCorrectAnswer){

                let newScore = ((score === 0) ? 0 : score);
                newScore++;
                setScore(newScore);

            }

            dispatch({

                type: "ADD_SCORE",
                payload: score

            });

            const quizLocalStorage = JSON.parse(localStorage.getItem("quizApp"));

            const scoreData = quizLocalStorage.scores;

            const newScoreData = scoreData.concat({
                name: store.getState().name,
                score: score,
                difficulty: store.getState().difficulty,
                category: store.getState().category,
                time: new Date()
            });

            const newQuizLocalStorage = {...quizLocalStorage, scores: newScoreData};

            localStorage.setItem("quizApp", JSON.stringify(newQuizLocalStorage));

            navigate("/score");
            
        }

    }

    return (

        <>
        <nav className="nav">

            <ul className="nav-list">

                <li className="nav-link">
                    <Link className="link" to="/">Home</Link>
                </li>

                <li className="nav-name">{store?.getState()?.name}</li>

            </ul>

        </nav>

        <div className="quiz">

            <div className="quiz-header">

                <div className="question-number">Question : {currentQuestionNumber} / 10</div>

                <div className="question-meta">
                    <div className="question-category">Category : "{currentQuestionData?.category}"</div>
                    <div className="question-difficulty">Difficulty : "{currentQuestionData?.difficulty}"</div>
                </div>
            
            </div>

            

            <div className="quiz-container">
                
                <div className="question-container"><div className="question">Q.</div>{" "}<p className="question-content">{currentQuestionData ? currentQuestionData?.question : "Question Loading"}</p></div>

                <div className="answer-container">

                    <div className="answer">

                        <div className="answer-card" onClick={() => {(currentAnswers.length  > 0) && handleAnswerSelection(currentAnswers[0])}}>
                            {(currentAnswers?.length > 0) ? currentAnswers[0] : "Option #1"}
                        </div>

                        <div className="answer-card" onClick={() => {(currentAnswers.length  > 0) && handleAnswerSelection(currentAnswers[1])}}>
                            {(currentAnswers?.length > 0) ? currentAnswers[1] : "Option #2"}
                        </div>

                        <div className="answer-card" onClick={() => {(currentAnswers.length  > 0) && handleAnswerSelection(currentAnswers[2])}}>
                            {(currentAnswers?.length > 0) ? currentAnswers[2] : "Option #3"}
                        </div>

                        <div className="answer-card" onClick={() => {(currentAnswers.length  > 0) && handleAnswerSelection(currentAnswers[3])}}>
                            {(currentAnswers?.length > 0) ? currentAnswers[3] : "Option #4"}
                        </div>

                    </div>

                </div>
            
            </div>
            
        </div>
        </>

    );

}
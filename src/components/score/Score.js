import { useDispatch } from "react-redux";
import { useNavigate, Link } from "react-router-dom";
import store from "../../store";

export default function Score (props) {

    const dispatch = useDispatch();
    const navigate = useNavigate();

    let imgTemplate = (
        <img src={process.env.PUBLIC_URL+"images/score0.png"} alt="Score Template Pic" className="score-img" />
    );

    const name = store.getState().name;
    const score = store.getState().score;

    function handlePlayAgainAction () {

        dispatch({

            type: "PLAY_AGAIN"

        });

        navigate("/quiz");

    }

    function handleNewQuizAction () {

        dispatch({

            type: "NEW_QUIZ"

        });

        navigate("/");

    }

    switch (score) {

        case 1:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score4_1.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 2:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score4_1.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 3:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score4_1.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 4:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score4_1.png"} alt="Score Template Pic" className="score-img" />
            );
            break;

        case 5:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score5.jpeg"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 6:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score6.png"} alt="Score Template Pic" className="score-img" />
            );
            break;

        case 7:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score7.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 8:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score8.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        case 9:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score9.png"} alt="Score Template Pic" className="score-img" />
            );
            break;

        case 10:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/win-img.png"} alt="Score Template Pic" className="score-img" />
            );
            break;
        
        default:
            imgTemplate = (
                <img src={process.env.PUBLIC_URL+"images/score6.png"} alt="Score Template Pic" className="score-img" />
            );
            break;

    }

    return (

        <>
        <nav className="nav">

            <ul className="nav-list">

                <li className="nav-link">
                    <Link className="link" to="/">Home</Link>
                </li>

                <li className="nav-name">{store?.getState()?.name}</li>

            </ul>

        </nav>

        <div className="score-container">
            
            <div className="score-card">

                <div className="name-content">
                    {name}
                </div>

                <h1 className="score-header">Score Card</h1>

                <div className="score-img-container">
                    {imgTemplate}
                </div>



                <div className="score-content">
                    Your Score : <span className="underline">{score ? score : 0}</span> out of 10
                </div>

                <div className="score-btn-container">
                    <button className="score-btn" onClick={handlePlayAgainAction}>Play Again</button>
                    <button className="score-btn" onClick={handleNewQuizAction}>New Quiz</button>
                </div>

            </div>

        </div>
        </>

    );

}
import axios from "axios";

const reducer = (state = {}, action) => {

    let newState = {...state};

    switch (action.type) {

        case "ADD_NAME":
            newState = {...state, name: action.payload};
            return newState;

        case "ADD_CATEGORY":
            newState = {...state, category: action.payload};
            return newState;

        case "ADD_DIFFICULTY":
            newState = {...state, difficulty: action.payload};
            return newState;
            
        case "ADD_SCORE":
            newState = {...state, score: action.payload};
            return newState;

        case "PLAY_AGAIN":
            let newQuestions = [];
            function getQuestions () {
                const category = state.category;
                const difficulty = state.difficulty;
    
                let url = "https://opentdb.com/api.php?amount=10";
    
                if(category !== "random"){
                    url += `&category=${category}`;
                }
                if(difficulty !== "random"){
                    url += `&difficulty=${difficulty}`;
                }
                url += `&type=multiple`;
                setTimeout(async() => {
                    const res = await (await axios.get(url)).data;
                    newQuestions = await res.results;
                },500);

            }
            getQuestions();
            setTimeout(()=>{
                newState = {
                    name: state.name,
                    category: state.category,
                    difficulty: state.difficulty,
                    questions: newQuestions
                };
            },500);
            return newState;

        case "NEW_QUIZ":
            newState = {};
            return newState;

        case "ADD_QUESTIONS":
            newState = {...state, questions: action.payload};
            return newState;

        default:
            return newState;

    }

}

export default reducer;